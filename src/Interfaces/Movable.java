package Interfaces;

import Enums.Direction;
import sample.Position;

public interface Movable extends Tile {
	void move(Direction dir);
	Position getPosition();
}
