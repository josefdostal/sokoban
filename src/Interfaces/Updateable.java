package Interfaces;

public interface Updateable {
	void update();
}
