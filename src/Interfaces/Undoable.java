package Interfaces;

import sample.Position;

public interface Undoable {
	void setPosition(Position pos);
	void setPosition(int x, int y);
	Position getPosition();
}
