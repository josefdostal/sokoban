package Interfaces;

public interface Tile {
	int tileSize = 64;
	void destroy();
}
