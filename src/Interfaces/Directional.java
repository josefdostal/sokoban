package Interfaces;

import Enums.Direction;

public interface Directional extends Tile {
	Direction getDirection();
	void setDirection(Direction dir);
}
