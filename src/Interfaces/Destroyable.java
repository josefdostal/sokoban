package Interfaces;

public interface Destroyable {
	void destroy();
}
