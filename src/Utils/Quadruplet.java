package Utils;

public class Quadruplet<T1, T2, T3, T4> {
	private T1 first;
	private T2 second;
	private T3 third;
	private T4 fourth;

	public Quadruplet(T1 first, T2 second, T3 third, T4 fourth) {
		this.first = first;
		this.second = second;
		this.third = third;
		this.fourth = fourth;
	}

	public T1 first() {
		return first;
	}

	public T2 second() {
		return second;
	}

	public T3 third() {
		return third;
	}

	public T4 fourth() {
		return fourth;
	}
}
