package Buttons;

import javafx.scene.layout.Pane;
import sample.Board;
import sample.Game;
import sample.Main;

public class StartButton extends MyButton {
	private Game g;
	private Board board;

	public StartButton(Pane root, Board board, Game g) {
		super("Start game", root);
		this.g = g;
		this.board = board;
	}

	@Override
	public void action() {
		g.start();
		Main.getStage().setScene(Main.getRoot().getScene());
	}
}
