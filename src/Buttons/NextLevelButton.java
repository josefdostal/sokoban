package Buttons;

import Exceptions.NoMoreLevelsException;
import Menus.WonMenu;
import javafx.scene.layout.Pane;
import sample.Board;
import sample.Game;
import sample.Main;

public class NextLevelButton extends MyButton {
	private Board board;
	private Game g;

	public NextLevelButton(Pane root, Board board, Game g) {
		super("Next level", root);
		this.g = g;
		this.board = board;
	}

	@Override
	public void action() {
		try {
			Main.getStage().setScene(Main.getRoot().getScene());
			this.board.nextLevel();
			g.start();
		} catch (NoMoreLevelsException e) {
			new WonMenu(board, g);
		}
	}
}
