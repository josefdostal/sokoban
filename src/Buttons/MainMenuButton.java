package Buttons;

import Menus.MainMenu;
import javafx.scene.layout.Pane;
import sample.Board;
import sample.Game;

public class MainMenuButton extends MyButton {
	private Board board;
	private Game g;

	public MainMenuButton(Pane root, Board board, Game g) {
		super("Main menu", root);
		this.board = board;
		this.g = g;
	}

	@Override
	public void action() {
		new MainMenu(board, g);
	}
}
