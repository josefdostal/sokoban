package Buttons;

import javafx.scene.layout.Pane;

public class QuitButton extends MyButton {
	private Pane root;

	public QuitButton(Pane root) {
		super("Quit Game", root);
		this.root = root;
	}

	@Override
	public void action() {
		System.exit(0);
	}
}
