package Buttons;

import Exceptions.NoMoreLevelsException;
import javafx.scene.Cursor;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import sample.SoundPlayer;

import java.io.File;

public abstract class MyButton {
	private Text text;
	private Rectangle rect;
	private StackPane sp;
	private int fontSize = 30;

	public MyButton(String text, Pane root) {
		this.text = new Text();
		this.text.setText(text);
		textAppearence();

		this.rect = new Rectangle();
		rectAppearence();

		sp = new StackPane();
		sp.getChildren().addAll(this.rect, this.text);
		sp.setTranslateX((root.getWidth()-getRect().getWidth())/2);
		sp.setTranslateY(0);
		sp.setOnMouseClicked(event -> action());
		sp.setCursor(Cursor.HAND);
		sp.setOnMouseEntered(event -> {
			this.rect.setStrokeWidth(5);

			new SoundPlayer().playSound();
		});
		sp.setOnMouseExited(event -> this.rect.setStrokeWidth(3));
		root.getChildren().add(sp);
	}

	private void textAppearence() {
		this.text.setFont(Font.font("Monospace", FontWeight.BOLD, fontSize));
		this.text.setFill(Color.valueOf("#bc8700"));
	}

	private void rectAppearence() {
		this.rect.setWidth(200);
		this.rect.setHeight(50);
		this.rect.setFill(Color.valueOf("#ffe45e"));
		this.rect.setArcWidth(20);
		this.rect.setArcHeight(20);
		this.rect.setStroke(Color.valueOf("#7a7a7a"));
		this.rect.setStrokeWidth(3);
	}

	public void setText(String text) {
		this.text.setText(text);
	}

	public Text getText() {
		return this.text;
	}

	public Rectangle getRect() {
		return this.rect;
	}

	public void setSize(long w, long h) {
		rect.setWidth(w);
		rect.setHeight(h);
	}

	public void setY(long y) {
		sp.setTranslateY(y);
	}

	public void setX(long x) {
		sp.setTranslateX(x);
	}

	public abstract void action();
}
