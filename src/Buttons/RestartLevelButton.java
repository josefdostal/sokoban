package Buttons;

import Exceptions.NoMoreLevelsException;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import sample.Board;
import sample.Game;
import sample.Main;

public class RestartLevelButton extends MyButton {
	private Board board;
	private Game g;

	public RestartLevelButton(Pane root, Board board, Game g) {
		super("Play again", root);
		this.g = g;
		this.board = board;
	}

	@Override
	public void action() {
		g.start();
		this.board.restart();
		Main.getStage().setScene(Main.getRoot().getScene());
	}
}
