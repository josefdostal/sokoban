package Buttons;

import javafx.scene.layout.Pane;
import sample.Game;

public class PlayAgainButton extends MyButton {
	private Pane root;

	public PlayAgainButton(Pane root) {
		super("Play Again", root);
	}

	@Override
	public void action() {
		Game g = new Game();
		g.start();
	}
}
