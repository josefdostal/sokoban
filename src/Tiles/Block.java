package Tiles;

import Interfaces.Destroyable;
import Interfaces.Tile;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import sample.Main;

public class Block extends Rectangle implements Tile, Destroyable {
	private Pane root = Main.getStRoot();

	public Block(int x, int y) {
		setWidth(tileSize);
		setHeight(tileSize);
		setFill(new ImagePattern(new Image("block.png")));
		setTranslateX(x*tileSize);
		setTranslateY(y*tileSize);
		root.getChildren().add(this);
	}

	public void destroy() {
		root.getChildren().remove(this);
	}
}
