package Tiles;

import Interfaces.*;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import Enums.Direction;
import sample.Main;
import sample.Position;

import static Enums.Direction.*;

public class Player extends Rectangle implements Tile, Movable, Directional, Undoable, Destroyable, Updateable {
	private Pane root = Main.getRoot();
	private double cooldown = 0.5;
	private static final double coolTime = 0.5;
	private Direction direction;
	private Position pos;

	public Player(int x, int y) {
		pos = new Position(x, y);
		setWidth(tileSize);
		setHeight(tileSize);
		direction = DOWN;
		update();
		root.getChildren().add(this);
	}

	public void update() {
		cooldown += 0.016;
		setTranslateX(pos.x*tileSize);
		setTranslateY(pos.y*tileSize);
		setFill(new ImagePattern(new Image("player_"+direction.toString()+".png")));
	}

	@Override
	public void move(Direction dir) {
		cooldown = 0;
		direction = dir;
		switch (dir) {
			case UP:
				pos.y -= 1;
				break;
			case DOWN:
				pos.y += 1;
				break;
			case RIGHT:
				pos.x += 1;
				break;
			case LEFT:
				pos.x -= 1;
				break;
		}
	}

	public void setDirection(Direction dir) {
		direction = dir;
	}

	public Direction getDirection() {
		return direction;
	}

	public Position getPosition() {
		return pos.clone();
	}

	@Override
	public void setPosition(Position pos) {
		this.pos = pos;
	}

	@Override
	public void setPosition(int x, int y) {
		pos.x = x;
		pos.y = y;
	}

	public void resetCooldown() {
		cooldown = 2;
	}

	public boolean isCool() {
		return cooldown > coolTime;
	}

	@Override
	public void destroy() {
		root.getChildren().remove(this);
	}
}
