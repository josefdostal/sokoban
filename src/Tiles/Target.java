package Tiles;

import Interfaces.Tile;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import sample.Main;

public class Target extends Rectangle implements Tile {
	private Pane root = Main.getStRoot();

	public Target(int x, int y) {
		setWidth(tileSize);
		setHeight(tileSize);
		setFill(new ImagePattern(new Image("target.png")));
		setTranslateX(x*tileSize);
		setTranslateY(y*tileSize);
		root.getChildren().add(this);
	}

	@Override
	public void destroy() {
		root.getChildren().remove(this);
	}
}
