package Tiles;

import Enums.BoxType;
import Interfaces.*;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import Enums.Direction;
import sample.Main;
import sample.Position;

public class Box extends Rectangle implements Tile, Movable, Undoable, Updateable, Destroyable {
	private Pane root = Main.getRoot();
	private BoxType type;
	private Position pos;

	public Box(int x, int y, BoxType type) {
		pos = new Position(x, y);
		setWidth(tileSize);
		setHeight(tileSize);
		this.type = type;
		update();
		root.getChildren().add(this);
	}

	public void update() {
		setTranslateX(getPosition().x*tileSize);
		setTranslateY(getPosition().y*tileSize);
		setFill(type.getImage());
	}

	@Override
	public void move(Direction dir) {
		switch (dir) {
			case UP:
				pos.y -= 1;
				break;
			case DOWN:
				pos.y += 1;
				break;
			case RIGHT:
				pos.x += 1;
				break;
			case LEFT:
				pos.x -= 1;
				break;
		}
	}

	@Override
	public Position getPosition() {
		return pos.clone();
	}

	public void setPosition(Position pos) {
		this.pos = pos;
	}

	public void setPosition(int x, int y) {
		pos.x = x;
		pos.y = y;
	}

	public void setType(BoxType type) {
		this.type = type;
	}

	public BoxType getType() {
		return this.type;
	}

	public void destroy() {
		root.getChildren().remove(this);
	}
}
