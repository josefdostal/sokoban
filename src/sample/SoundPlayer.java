package sample;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.io.File;

public class SoundPlayer {
	private MediaPlayer player;

	public SoundPlayer() {
		player = new MediaPlayer(
				new Media(new File("sound/newboing.wav").toURI().toString()));
	}

	public void playSound() {
		if (!Settings.isMute())
			player.play();
	}
}
