package sample;

import Enums.Direction;
import Exceptions.NoMoreRecordsException;
import Interfaces.Directional;
import Interfaces.Undoable;
import Utils.Tuple;

import java.util.ArrayList;
import java.util.Collections;

// TODO box state remains after undo
public class Undoer {
	private static final int MAX_RECORDS = 10;
	private ArrayList<Undoable> undoables;
	private ArrayList<Tuple<Position, Direction>> dirPoses;
	private ArrayList<ArrayList<Position>> nonDirPoses;

	public Undoer(ArrayList<Undoable> l) {
		undoables = l;
		dirPoses = new ArrayList<>();
		nonDirPoses = new ArrayList<>();
	}

	public void newRecord() {
		ArrayList<Position> boxPoses = new ArrayList<>();
		for (Undoable u : undoables) {
			if (u instanceof Directional)
				dirPoses.add(new Tuple<>(u.getPosition(), ((Directional)u).getDirection()));
			boxPoses.add(u.getPosition());
		}
		this.nonDirPoses.addAll(Collections.singleton(boxPoses));
		if (dirPoses.size() > MAX_RECORDS) { // == boxPoses.size()
			dirPoses.remove(0);
			this.nonDirPoses.remove(0);
		}
	}

	public void undo() throws NoMoreRecordsException {
		if (dirPoses.size() > 0) { // == boxPoses.size()
			for (int i = 0 ; i < undoables.size() ; i++) {
				if (undoables.get(i) instanceof Directional) {
					undoables.get(i).setPosition(dirPoses.get(dirPoses.size() - 1).first());
					((Directional)undoables.get(i)).setDirection(dirPoses.get(dirPoses.size() - 1).second());
				}
				undoables.get(i).setPosition(nonDirPoses.get(nonDirPoses.size() - 1).get(i));
			}
			dirPoses.remove(dirPoses.size() - 1);
			nonDirPoses.remove(nonDirPoses.size() - 1);
		}
		else
			throw new NoMoreRecordsException();
	}
}
