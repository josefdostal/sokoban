package sample;

public class Position {
	public int x;
	public int y;
	public Position() { }
	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Position clone() {
		return new Position(x, y);
	}
}
