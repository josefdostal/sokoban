package sample;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class MoveCounter extends Text {
	private Pane root = Main.getRoot();
	private int count;
	private final int size = 30;

	public MoveCounter() {
		count = 0;
		setFont(Font.font("Monospace", FontWeight.BOLD, 30));
		setFill(Color.valueOf("#f2ad00"));
		update();
		setTranslateX(Settings.PANE_WIDTH-getLayoutBounds().getWidth()-5);
		setTranslateY(Settings.PANE_HEIGHT-50);
		root.getChildren().add(this);
	}

	public int getCount() {
		return count;
	}

	public void inc() {
		if (count < 1000) {
			count++;
			update();
		}
	}

	public void dec() {
		if (count > 0) {
			count--;
			update();
		}
	}

	private void update() {
		setText("Moves: " + String.format("%03d", count));
	}
}
