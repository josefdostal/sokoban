package sample;

import Enums.OnOff;
import javafx.scene.Cursor;
import javafx.scene.shape.Rectangle;

import static sample.Settings.sound;

public class SoundBar extends Rectangle {
	public SoundBar() {
		setFill(sound.getImage());
		setWidth(40);
		setHeight(40);
		setTranslateX(Settings.PANE_WIDTH - 60);
		setTranslateY(20);
		setOnMouseClicked(event -> {
			changeState();
		});
		setCursor(Cursor.HAND);
	}

	public void changeState() {
		if (sound == OnOff.ON)
			sound = OnOff.OFF;

		else
			sound = OnOff.ON;
		setFill(sound.getImage());
	}
}
