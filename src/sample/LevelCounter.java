package sample;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class LevelCounter extends Text {
	private Pane root = Main.getSysRoot();
	private int level;
	private final int size = 30;

	public LevelCounter() {
		level = 1;
		setFont(Font.font("Monospace", FontWeight.BOLD, size));
		setFill(Color.valueOf("#f2ad00"));
		update();
		root.getChildren().add(this);
	}

	public void update() {
		String text = "Level: " + level;
		setTranslateX((Settings.PANE_WIDTH-(text.length()*25))/2);
		setTranslateY(Settings.PANE_HEIGHT-5);
		setText(text);
	}

	public void inc() {
		level++;
		update();
	}

	public void dec() {
		level--;
		update();
	}

	public void setLevel(int level) {
		this.level = level;
	}
}
