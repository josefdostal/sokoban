package sample;

import Enums.OnOff;


public class Settings {
	public static final double PANE_WIDTH = 940;
	public static final double PANE_HEIGHT = 580;
	public static OnOff sound = OnOff.ON;

	public static boolean isMute() {
		if (sound == OnOff.ON)
			return false;
		else
			return true;
	}
}
