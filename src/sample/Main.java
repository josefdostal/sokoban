package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application {
	private static Pane root = new Pane();
	private static StackPane stRoot = new StackPane();
	private static Pane sysRoot = new Pane();
	private static Stage stage;
	private static StackPane stackPane = new StackPane();

	private Parent createContent() {
		stackPane.getChildren().addAll(stRoot);
		Game g = new Game();
		root.setPrefSize(Settings.PANE_WIDTH, Settings.PANE_HEIGHT);
		g.start();

		root.getChildren().addAll(stackPane);
		root.getChildren().addAll(sysRoot);
		root.getChildren().add(new SoundBar());
		return root;
	}

	public static Pane getStRoot() {
		return stRoot;
	}

	public static Pane getRoot() {
		return stackPane;
	}

	public static Stage getStage() {
		return stage;
	}

	public static Pane getSysRoot() {
		return sysRoot;
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		stage = primaryStage;
		root = FXMLLoader.load(getClass().getResource("sample.fxml"));
		stage.setTitle("Sokoban");
		Scene scene = new Scene(createContent());
		stage.setScene(scene);
		stage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
