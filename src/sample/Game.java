package sample;

import Menus.EndLevelMenu;
import Menus.MainMenu;
import javafx.animation.AnimationTimer;
import javafx.scene.layout.Pane;

public class Game extends AnimationTimer {
	private Pane root = Main.getRoot();
	private Pane stRoot = Main.getStRoot();

	private Board board;
	private int width;
	private int height;
	private boolean won;
	private boolean first;

	public Game() {
		board = new Board();
		width = board.getWidth();
		height = board.getHeight();
		first = true;
	}

	@Override
	public void handle(long now) {
		if (first) {
			new MainMenu(board, this);
			first = false;
		}
		boolean wasHere = false;
		// TODO "won menu"
		if (won) {
			stop();
			new EndLevelMenu(board, this);
			wasHere = true;
		}
		won = board.update();
		if (wasHere)
			won = false;
	}

	public void updateSize() {
		this.width = board.getWidth();
		this.height = board.getHeight();
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public boolean isWon() {
		return won;
	}
}
