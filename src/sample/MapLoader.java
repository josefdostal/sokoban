package sample;

import Exceptions.NoMoreLevelsException;

import java.io.*;
import java.util.ArrayList;

public class MapLoader {
	private int width;
	private int height;
	private ArrayList<ArrayList<Character>> board;
	private int level;

	public MapLoader() {
		this.board = new ArrayList<>();
		this.level = 1;
		try {
			loadLevel();
		} catch (NoMoreLevelsException e) {
			e.noLevelMsg();
		}
	}

	private void loadLevel() throws NoMoreLevelsException {
		FileReader f = openFile("maps/" + level + ".smp");
		ArrayList<Character> tmp;
		while ((tmp = readLine(f)) != null) {
			board.add(tmp);
		}

		try {
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		setHeight();
		setWidth();
	}

	private ArrayList<Character> readLine(FileReader f) throws NoMoreLevelsException {
		int c;
		ArrayList<Character> tmp = new ArrayList<>();

		// in returns -1 as first char, its eof and map is loaded
		try {
			if ((c = f.read()) == -1)
				return null;
			else
				tmp.add((char) c);

			// otherwise it continues reading lines
			while (true) {
				if ((c = f.read()) == (int)'\n' | c == -1)
					return tmp;
				tmp.add((char)c);
			}
		} catch (IOException e) {
			throw new NoMoreLevelsException();
		}
	}

	private FileReader openFile(String fileName) throws NoMoreLevelsException {
		try {
			return new FileReader(fileName);
		} catch (FileNotFoundException e) {
			throw new NoMoreLevelsException();
		}
	}

	public ArrayList<ArrayList<Character>> getLevel() {
		return this.board;
	}

	public void nextLevel() throws NoMoreLevelsException {
		this.level++;
		board = new ArrayList<>();
		loadLevel();
	}

	private void setHeight() {
		this.height = this.board.size();
	}

	private void setWidth() {
		int max = 0;
		for (ArrayList<Character> line : board) {
			if (line.size() > max)
				max = line.size();
		}
		this.width = max;

		for (ArrayList<Character> line : board) {
			while (line.size() < this.width) {
				line.add(' ');
			}
		}
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public void setLevel(int level) {
		this.level = level;
	}

}
