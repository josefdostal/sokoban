package sample;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class Timer extends Text {
	private Pane root = Main.getRoot();
	private long startTime;
	private long time;
	private final int size = 30;

	public Timer() {
		startTime = System.nanoTime();
		setFont(Font.font("Monospace", FontWeight.BOLD, size));
		setFill(Color.valueOf("#f2ad00"));
		setTranslateX(5);
		setTranslateY(Settings.PANE_HEIGHT - 50);
		update();
		root.getChildren().add(this);
	}

	public void update() {
		time = (long) ((System.nanoTime()-startTime)/1e9);
		setText(String.format("%02d", time/60) + ":" + String.format("%02d", time%60));
	}

	public void show() {
		System.out.println((System.nanoTime()-startTime)/1e9);
	}

	public double getTime() {
		return ((System.nanoTime() - this.startTime)/1e9);
	}
}
