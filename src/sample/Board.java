package sample;

import Enums.BoxType;
import Enums.Direction;
import Exceptions.NoMoreLevelsException;
import Exceptions.NoMoreRecordsException;
import Interfaces.Tile;
import Interfaces.Undoable;
import Interfaces.Updateable;
import Tiles.*;
import Utils.Quadruplet;
import Utils.Tuple;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;

import java.util.ArrayList;

public class Board {
	private Pane root = Main.getRoot();
	private Pane stRoot = Main.getStRoot();
	private Pane sysRoot = Main.getSysRoot();
	private Tile[][] board;
	private Player player;
	private ArrayList<Box> boxes;
	private int width;
	private int height;
	private KeyCode old_key = null;
	private Undoer undoer;
	private MapLoader mapLoader;
	private MoveCounter moveCounter;
	private Timer timer;
	private LevelCounter levelCounter;

	public Board() {
		boxes = new ArrayList<>();
		mapLoader = new MapLoader();
		loadBoard();
		ArrayList<Undoable> undoables = new ArrayList<>();
		undoables.add(player);
		undoables.addAll(boxes);
		undoer = new Undoer(undoables);
		moveCounter = new MoveCounter();
		timer = new Timer();
		levelCounter = new LevelCounter();
	}

	public boolean update() {
		handleKey();
		boxTypeUpdate();
		root.getChildren().filtered(n -> n instanceof Updateable).forEach(n -> {
			((Updateable)n).update();
		});
		timer.update();
		return allOk();
	}

	// TODO do something with the Destroyable interface
	public void restart() {
		boxes = new ArrayList<>();
		root.getChildren().clear();
		root.getChildren().addAll(stRoot);
		root.getChildren().addAll(sysRoot);
		loadBoard();
		undoer = new Undoer(getUndoables());
		moveCounter = new MoveCounter();
		timer = new Timer();
	}

	private void boxTypeUpdate() {
		Position pos;
		for (Box b : boxes) {
			pos = b.getPosition();
			if (board[pos.x][pos.y] instanceof Target)
				b.setType(BoxType.OK);
			else
				b.setType(BoxType.NOK);
		}
	}

	private void handleKey() {
		Scene scene = root.getScene();
		scene.setOnKeyPressed(e -> {
			if (e.getCode() != old_key) {
				player.resetCooldown();
				old_key = e.getCode();
			}
			beforeMove(e);
			switch (e.getCode()) {
				case UP:
				case DOWN:
				case RIGHT:
				case LEFT:
				case K:
				case J:
				case L:
				case H:
					move(Direction.val(e.getCode().getName()));
					break;
				case U:
					try {
						undoer.undo();
						moveCounter.dec();
					} catch (NoMoreRecordsException ex) {
						//ignored
					}
					break;
				case R:
					restart();
					break;
				default:
					break;
			}
		});
		scene.setOnKeyReleased(e -> {
			player.resetCooldown();
		});
	}

	private void beforeMove(KeyEvent e) {
		new SoundPlayer().playSound();
	}

	private void move(Direction dir) {
		if (player.isCool()) {
			int moveType = canMove(dir);
			if (moveType >= 1) {
				undoer.newRecord();
				moveCounter.inc();
			}
			if (moveType >= 2) {
				Tuple tmp = nextBox(dir);
				assert tmp != null;
				Box b = getBox((int)tmp.first(), (int)tmp.second());
				assert b != null;
				b.move(dir);
			}
			if (moveType >= 1) {
				player.move(dir);
			}
			if (moveType == 0) {
				player.setDirection(dir);
			}
		}
	}

	private Tuple<Integer, Integer> nextBox(Direction dir) {
		Position pos = player.getPosition();
		switch(dir) {
			case UP:
				return new Tuple<>(pos.x, pos.y-1);
			case DOWN:
				return new Tuple<>(pos.x, pos.y+1);
			case RIGHT:
				return new Tuple<>(pos.x+1, pos.y);
			case LEFT:
				return new Tuple<>(pos.x-1, pos.y);
			default:
				return null;
		}
	}

	private int canMove(Direction dir) {
		Quadruplet q = getNexts(dir);
		assert q != null;
		if (!(board[(int)q.first()][(int)q.third()] instanceof Block)) {
			if (getBox((int)q.first(), (int)q.third()) != null) {
				if (!(board[(int)q.second()][(int)q.fourth()] instanceof Block) & getBox((int)q.second(), (int)q.fourth()) == null) {
					return 2;
				}
				return 0;
			}
			return 1;
		}
		return 0;
	}

	private Quadruplet<Integer, Integer, Integer, Integer> getNexts(Direction direction) {
		int x = player.getPosition().x;
		int y = player.getPosition().y;
		switch (direction) {
			case UP:
				return new Quadruplet<>(x, x, y-1, y-2);
			case DOWN:
				return new Quadruplet<>(x, x, y+1, y+2);
			case RIGHT:
				return new Quadruplet<>(x+1, x+2, y, y);
			case LEFT:
				return new Quadruplet<>(x-1, x-2, y, y);
			default:
				return null;
		}
	}

	private Box getBox(int x, int y) {
		for (Box b : boxes)
			if (b.getPosition().x == x && b.getPosition().y == y)
				return b;
		return null;
	}

	private boolean allOk() {
		for (Box b : boxes) {
			if (b.getType() != BoxType.OK) {
				return false;
			}
		}
		return true;
	}

	private void loadBoard() {
		ArrayList<ArrayList<Character>> b = mapLoader.getLevel();
		this.width = mapLoader.getWidth();
		this.height = mapLoader.getHeight();
		board = new Tile[width][height];

		for (int i = 0 ; i < width ; i++) {
			for (int j = 0 ; j < height ; j++) {
				switch (b.get(j).get(i)) {
					case '@':
						player = new Player(i, j);
						break;
					case '$':
						Box tmp = new Box(i, j, BoxType.NOK);
						boxes.add(tmp);
						break;
					case '&':
						tmp = new Box(i, j, BoxType.OK);
						boxes.add(tmp);
						board[i][j] = new Target(i, j);
						break;
					case 'x':
						board[i][j] = new Target(i, j);
						break;
					case '#':
						board[i][j] = new Block(i, j);
						break;
					default:
						board[i][j] = null;
						break;
				}
			}
		}
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	// TODO clean this mess
	public void nextLevel() throws NoMoreLevelsException {
		for (Box b : boxes)
			b.destroy();
		player.destroy();
		boxes = new ArrayList<>();
		for (Tile[] line : board)
			for (Tile t : line)
				if (t != null)
					t.destroy();
		root.getChildren().clear();
		stRoot.getChildren().clear();
		mapLoader.nextLevel();
		root.getChildren().addAll(stRoot);
		root.getChildren().addAll(sysRoot);
		levelCounter.inc();
		loadBoard();
		undoer = new Undoer(getUndoables());
		moveCounter = new MoveCounter();
		timer = new Timer();
	}

	public double getTime() {
		return timer.getTime();
	}

	public int getMoves() {
		return moveCounter.getCount();
	}

	public void setLevel(int level) {
		levelCounter.setLevel(1);
		mapLoader.setLevel(level);
	}

	private ArrayList<Undoable> getUndoables() {
		ArrayList<Undoable> tmp = new ArrayList<>();
		tmp.add(player);
		tmp.addAll(boxes);
		return tmp;
	}
}
