package Menus;

import Buttons.MainMenuButton;
import Buttons.QuitButton;
import sample.Board;
import sample.Game;

public class WonMenu extends MyMenu {

	public WonMenu(Board board, Game g) {
		super();
		setText("You won all levels");
		setTextPosY(150);
		QuitButton qb = new QuitButton(getRoot());
		qb.setY(450);
	}
}
