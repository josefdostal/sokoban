package Menus;

import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import sample.Main;
import sample.Settings;
import sample.SoundBar;

public abstract class MyMenu {
	private Text text;
	private Pane root;

	public MyMenu() {
		root = new Pane();
//		root.setStyle("-fx-background-color: rgba(255, 255, 255, 0);");
		root.setPrefSize(Settings.PANE_WIDTH, Settings.PANE_HEIGHT);
		Main.getStage().setScene(new Scene(root));
		text = new Text();
		textAppearence();
		root.getChildren().add(new SoundBar());
	}

	private void textAppearence() {
		root.getChildren().add(getText());

		text.setFont(Font.font("Monospace", FontWeight.BOLD, 50));
		text.setFill(Color.valueOf("#f2ad00"));
		centerText();
		text.setTranslateY(0);
		text.setStroke(Color.valueOf("#515151"));
		text.setStrokeWidth(3);
		text.setText("");
	}

	public void setTextPosX(long x) {
		text.setTranslateX(x);
	}

	public void setTextPosY(long y) {
		text.setTranslateY(y);
	}

	public void setTextPos(long x, long y) {
		text.setTranslateX(x);
		text.setTranslateY(y);
	}

	public void centerText() {
		text.setTranslateX((root.getWidth()-text.getText().length()*30)/2);
	}

	public void setText(String text) {
		this.text.setText(text);
		centerText();
	}

	public Text getText() {
		return text;
	}

	public Pane getRoot() {
		return root;
	}
}
