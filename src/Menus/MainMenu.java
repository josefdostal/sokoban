package Menus;

import Buttons.StartButton;
import sample.Board;
import sample.Game;

public class MainMenu extends MyMenu {

	public MainMenu(Board board, Game g) {
		super();
		setText("SOKOBAN");
		setTextPosY(200);
		StartButton sb = new StartButton(getRoot(), board, g);
		sb.setY(350);
	}
}
