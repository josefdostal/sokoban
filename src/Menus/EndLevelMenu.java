package Menus;

import Buttons.NextLevelButton;
import Buttons.QuitButton;
import Buttons.RestartLevelButton;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import sample.Board;
import sample.Game;

public class EndLevelMenu extends MyMenu {
	private Board board;

	public EndLevelMenu(Board board, Game g) {
		super();
		this.board = board;
		setText("Congratulations");
		setTextPosY(150);
		writeStats();
		RestartLevelButton rlb = new RestartLevelButton(getRoot(), board, g);
		rlb.setY(360);
		NextLevelButton nlb = new NextLevelButton(getRoot(), board, g);
		nlb.setY(420);
		QuitButton qb = new QuitButton(getRoot());
		qb.setY(510);
	}

	private void writeStats() {
		getRoot().getChildren().add(getTime());
		getRoot().getChildren().add(getMoves());
	}

	private Text getTime() {
		Text time = new Text();
		time.setText("Time:\n"+String.format("%02.0f:%02.0f", board.getTime()/60, board.getTime()%60));
		statsFormating(time, 100);
		return time;
	}

	private Text getMoves() {
		Text moves = new Text();
		moves.setText("Moves:\n"+String.format("%03d", board.getMoves()));
		statsFormating(moves, (getRoot().getWidth() - (moves.getText().length()*(30/5)*2) - 100));
		return moves;
	}

	private void statsFormating(Text text, double x) {
		text.setFont(Font.font("Monospace", FontWeight.BOLD, 30));
		text.setFill(Color.valueOf("#f2ad00"));
		text.setTranslateX(x);
		text.setTranslateY(230);
		text.setStroke(Color.valueOf("#515151"));
	}
}
