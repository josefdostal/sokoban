package Enums;

public enum Direction {
	UP, DOWN, RIGHT, LEFT;

	@Override
	public String toString() {
		return super.toString().toLowerCase();
	}

	public static Direction val(String value) {
		switch (value.toLowerCase()) {
			case "up":
			case "k":
				return UP;
			case "down":
			case "j":
				return DOWN;
			case "right":
			case "l":
				return RIGHT;
			case "left":
			case "h":
				return LEFT;
			default:
				return null;
		}
	}
}
