package Enums;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;

import java.awt.*;

public enum OnOff {
	ON(new ImagePattern(new Image("sound_on.png"))),
	OFF(new ImagePattern(new Image("sound_off.png")));

	private ImagePattern ip;

	OnOff(ImagePattern ip) {
		this.ip = ip;
	}

	public ImagePattern getImage() {
		return this.ip;
	}
}
