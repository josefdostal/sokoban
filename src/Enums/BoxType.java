package Enums;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;

import java.util.HashMap;
import java.util.Map;

public enum BoxType {
	NOK(0, new ImagePattern(new Image("box_nok.png"))),
	OK(1, new ImagePattern(new Image("box_ok.png")));

	private int value;
	private ImagePattern img;
	private static Map map = new HashMap<String, BoxType>();

	BoxType(int value, ImagePattern img) {
		this.value = value;
		this.img = img;
	}

	public ImagePattern getImage() {
		return img;
	}

	static {
		for (BoxType bt : BoxType.values()) {
			map.put(bt.value, bt);
		}
	}

	public static BoxType valueOf(int boxType) {
		return (BoxType) map.get(boxType);
	}

	public int getValue() {
		return value;
	}
}
