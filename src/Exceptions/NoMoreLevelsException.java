package Exceptions;

public class NoMoreLevelsException extends Exception {
	public NoMoreLevelsException() { }
	public void msg() {
		System.out.println("There are no more levels to go through");
	}
	public void noLevelMsg() {
		System.out.println("There are no levels to go through");
	}
}
