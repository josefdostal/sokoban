package Exceptions;

public class NoMoreRecordsException extends Exception {
	public NoMoreRecordsException() { }
	public void what() {
		System.out.println("There are no more records in undoer");
	}
}
